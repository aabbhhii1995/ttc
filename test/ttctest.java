import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ttctest {
	double price1 = 2.50 ;
	double price2 = 3.00 ;
	
	ttc calc = new ttc();
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void r1() {
		
		String[] a = {"lesli"};
		String [] b = {"don mill"};
		double result = calc.total(a, b);
		assertEquals(price1, result,0.0001);
		
	}
	
	@Test
	public void r2() {
		
		String[] a = {"finch"};
		String[] b = {"shepperd"};	
		double result = calc.total(a, b);
		
		assertEquals(price2, result,0.0001);
				
	}
	
	@Test
	public void r3() {
		
		String[] a = {"don mill"};
		String[] b = {"finch"};	
		double result = calc.total(a, b);		
		assertEquals(price2, result,0.0001);
				
	}
	
	@Test
	public void r4() {
		
		String[] a = {"finch","lesli"};
		String[] b = {"sheppard","don mill"};	
		double result = calc.total(a, b);
		double fprice1 = price1 + price2 ;
		assertEquals(fprice1, result,0.0001);
				
	}
	
	@Test
	public void r5() {
		
		String[] a = {"finch","sheppard","finch"};
		String[] b = {"sheppard","finch","sheppard"};	
		double result = calc.total(a, b);
		double fprice2 = price2 + price2 ;
		assertEquals(fprice2, result,0.0001);
				
	}
}
